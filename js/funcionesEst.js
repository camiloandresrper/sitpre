
async function leerDatosJSON(url){
    try{
        let response = await fetch(url);
        let estudent = await response.json();
        return estudent;
    } catch(err) {
        alert(err);
    }
}

function leerURL(){
    let codigo = parseInt(document.getElementById("codigo").value);
    let notas  =parseInt( document.getElementById("CantNotas").value);
window.location = (`html/mostrarNotas.html?codigo=${codigo}&cantBorrar=${notas}`);
}

function leerDatos(){
    const parametro = new URLSearchParams(window.location.search);
    let codigo = parametro.get("codigo");
    let notas  = parametro.get("cantBorrar");
    let url = "https://programacion-web---i-sem-2019.gitlab.io/persistencia/json_web/json/estudiantes.json";
    leerDatosJSON(url).then(datos => {
        InfoEstudiantes(datos.estudiantes, codigo, notas, datos.nombreMateria, datos.descripcion);
    });
}

 //Comparamos el codigo que traemos de la url con los del JSON para filtrar la informacion de ese estudiante
function InfoEstudiantes(estudiantes, codigo, notas, nombreMateria, descripcion){
    let estudiante = (estudiantes.filter(estudiante => estudiante.codigo == codigo))[0];
    dibujarTablaInfoEstudiante(estudiante, notas, nombreMateria);
    dibujarTablaNotas(estudiante, descripcion, notas);
    dibujarGrafica(estudiante.notas);
    
}

//Validamos si la nota esta aprobada o reprobada 
function validarNotas(notas){

    let array = [];
    let aprobado = 0, reprobada=0;
    for (let i = 0; i < notas.length; i++) {
        if(notas[i].valor < 3)  aprobado++;
        else reprobada++;
    }
    array[0] = aprobado;
    array[1] = reprobada;
    return array;
}

//Tabla con la info basica del estudiante
function dibujarTablaInfoEstudiante(estudiante, notas, nombreMateria){

    var data = new google.visualization.DataTable();
    data.addColumn("string");
    data.addRows([
        [`Nombre:${estudiante.nombre}`],
        [`Código:${estudiante.codigo}`],
        [`Materia:${nombreMateria}`],
    ]);

    var table = new google.visualization.Table(document.getElementById('table_div'));
    table.draw(data, {showRowNumber: false, width: '100%', height: '100%'});
}

//Tabla con las notas del estudiante
function dibujarTablaNotas(estudiante,descripciones, maxNotas){

    let notas = estudiante.notas;
    var data = new google.visualization.DataTable();
    data.addColumn("string","Descripcion");
    data.addColumn("number", "Valor");
    data.addColumn("string", "Observacion");
    data.addRows(notas.length+1);

    let observacion = calcularMensajes(notas,maxNotas);

    for (let i = 0; i < notas.length; i++) {
        data.setCell(i,0,descripciones[(notas[i].id)-1].descripcion);
        data.setCell(i,1,notas[i].valor);
        data.setCell(i,2,observacion[i]);
    }

    let promedioNotas = [...notas];
    promedioNotas.splice((notas.length-maxNotas),notas.length)

    let prome = promedio(promedioNotas); 
console.log(promedioNotas);
    //Se crea nueva fila para la informacion de la deficinitva
    data.setCell(notas.length,0,"NOTA DEFINITIVA");
    data.setCell(notas.length,1,prome.toFixed(2));
    data.setCell(notas.length,2,"NOTA DEFINITIVA");

    var table = new google.visualization.Table(document.getElementById('tableNotas_div'));
    table.draw(data, {showRowNumber: false, width: '100%', height: '100%'});
}


let promedio = (notas=>{
    let prom = 0;
    notas.forEach(nota=> {
        prom+= nota.valor;
    });
    return prom/notas.length
})

//Diseñamos el grafico PIE con el % trabajos  aprobados y reprobados
function dibujarGrafica(notas) {
    const parametro = new URLSearchParams(window.location.search);
    let notasEliminadas = parametro.get("cantBorrar");
    let array = validarNotas(notas);
    var data = google.visualization.arrayToDataTable([
        ["Estado","Porcentaje"],
        ["Aprobado",array[1]],
        ["Reprobada",array[0]-notasEliminadas]
        
    ]);
    var options = {
        title: '',
        is3D: true,
    };
    var chart = new google.visualization.PieChart(document.getElementById('pie_div'));
    chart.draw(data, options);
}

//Se definen las descripciones a asginar a las notas 
function calcularMensajes(notasMsg,maxNotas){
    let msg = [];
    notasMsg.sort((a, b) => b.valor - a.valor);
 
    console.log(notasMsg);
    for (let i = 0; i < notasMsg.length; i++){
        
            if(notasMsg[i].valor >= 3){
                msg[i] = "Nota Aprobada";
            }
            if(notasMsg[i].valor < 3){
                msg[i] = "Nota reprobada";
            }

            if(i>=notasMsg.length-maxNotas){
                msg[i] = "Nota Eliminada";
            }

    }

    return msg;
}


function mensaje() {  
    let codigo = parseInt(document.getElementById("codigo").value);
    console.log(codigo);
    let notasa  =parseInt( document.getElementById("CantNotas").value);
    alert("Mi Codigo :"+codigo+"  Se eliminarom: "+ notasa+" Notas  "+
    "   Para ver sus notas y definitiva ingrese a Consultar Notas"
    );

}